import 'package:mabank/data/datasource/profile_datasource.dart';
import 'package:mabank/domain/entity/profile_entity.dart';
import 'package:mabank/domain/mapper/profile_model_to_entity_mapper.dart';
import 'package:mabank/domain/repositories/profile_repository.dart';

class ProfileRepositoryImpl implements ProfileRepository {
  final ProfileDataSource _profileDataSource;

  ProfileRepositoryImpl(this._profileDataSource);
  @override
  Future<ProfileEntity>getProfile() {
    return _profileDataSource.getProfile().then(
      (value) {
        return  ProfileModelToEntityMapper(value).toEntity();
      },
    );
  }
}
