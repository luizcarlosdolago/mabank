import 'package:mabank/data/datasource/notification_data_source.dart';
import 'package:mabank/domain/entity/notification_entity.dart';
import 'package:mabank/domain/mapper/notification_model_to_entity_mapper.dart';
import 'package:mabank/domain/repositories/notification_repository.dart';

class NotificationREpositoryImpl implements NotificationRepository {
  final NotificationDataSource _notificationDataSource;

  NotificationREpositoryImpl(this._notificationDataSource);
  @override
  Future<List<NotificationEntity>> getNotification() {
    return _notificationDataSource.getNotification().then(
      (value) {
        return value
            .map((e) => NotificationModelToEntityMapper(e).toEntity())
            .toList();
      },
    );
  }
}
