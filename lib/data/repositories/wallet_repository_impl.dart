import 'package:mabank/data/datasource/wallet_data_source.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/mapper/wallet_model_to_entity_mapper.dart';
import 'package:mabank/domain/repositories/wallet_repository.dart';

import '../../domain/mapper/wallet_model_to_entity_mapper.dart';
import '../../domain/repositories/wallet_repository.dart';

class WalletRepositoryImpl implements WalletRepository {
  final WalletDataSource _walletDataSource;

  WalletRepositoryImpl(this._walletDataSource);
  @override
  Future<WalletEntity> getWallet() {
    return _walletDataSource.getWallet().then(
      (value) {
        return WalletModelToEntityMapper(value).toEntity();
      },
    );
  }
}
