import 'package:mabank/data/datasource/last_transaction_datasource.dart';
import 'package:mabank/domain/entity/last_transaction_entity.dart';
import 'package:mabank/domain/mapper/last_transaction_model_to_entity_mapper.dart';
import 'package:mabank/domain/repositories/last_transaction_repository.dart';

class LastTRansactionRepsitoryImpl implements LastTransactionRepository {
  final LastTransactionDataSource _lastTransactionDataSource;

  LastTRansactionRepsitoryImpl(this._lastTransactionDataSource);

  
  @override
  Future<List<LastTransactionEntity>> getLastTRansaction() {
    return _lastTransactionDataSource.getlastTransaction().then((value) {
      return value
          .map((e) => LastTransactionModelToEntityMapper(e).toEntity())
          .toList();
    });
  }
}
