import 'package:mabank/data/model/profile_model.dart';

abstract class ProfileDataSource {
  Future<ProfileModel> getProfile();
}
