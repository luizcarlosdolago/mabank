
import '../model/wallat_model.dart';

abstract class WalletDataSource {
  Future<WalletModel> getWallet();
}
