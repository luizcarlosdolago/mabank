import 'package:mabank/data/datasource/notification_data_source.dart';
import 'package:mabank/data/model/notification_model.dart';

class NotificationMockImpl implements NotificationDataSource {
  @override
  Future<List<NotificationModel>> getNotification() {
    return Future.delayed(const Duration(seconds: 4)).then((value) {
      List<NotificationModel> notification = [];

      notification.add(
        NotificationModel(
          date: "29 June 2021, 7.14 PM",
          wasRead: false,
          type: "‘Pay debt’",
          icon: 'assets/icon/arrowtop.png',
          message: 'You received Rp100.000 from Alexandr Gibson Jogja',
        ),
      );
      notification.add(
        NotificationModel(
          date: "29 June 2021, 9.02 AM",
          wasRead: false,
          type: "‘D’",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp32.000 for Coffe Cetar back Tugu Sentral',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "29 June 2021, 7.14 PM",
          wasRead: true,
          type: "‘Pay debt’",
          icon: 'assets/icon/arrowtop.png',
          message: 'You received Rp100.000 from Alexandr Gibson Jogja',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );
      notification.add(
        NotificationModel(
          date: "28 June 2021, 8.32 PM",
          wasRead: true,
          type: "D",
          icon: 'assets/icon/arrow.png',
          message: 'You spent Rp210.000 for pay Tokosbla ijo mera',
        ),
      );

      notification.sort((a, b) => b.date.compareTo(a.date));
      return notification;
    });
  }
}
