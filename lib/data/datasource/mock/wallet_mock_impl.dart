import 'package:mabank/data/datasource/wallet_data_source.dart';
import 'package:mabank/data/model/wallat_model.dart';

class WalletMockImpl implements WalletDataSource {
  @override
  Future<WalletModel> getWallet() {
    return Future.delayed(const Duration(seconds: 4)).then(
      (value) {
        return WalletModel(
            status: "Active", balance: "\$1.234", brand: 'MasterCard');
      },
    );
  }
}
