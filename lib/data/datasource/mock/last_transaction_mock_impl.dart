import 'package:flutter/material.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/data/datasource/last_transaction_datasource.dart';
import 'package:mabank/data/model/last_transaction_model.dart';

class LastTransactionMockImpl implements LastTransactionDataSource {
  @override
  Future<List<LastTransactionModel>> getlastTransaction() {
    return Future.delayed(const Duration(seconds: 4)).then(
      (value) {
        List<LastTransactionModel> transactions = [];
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/netflix.png",
            title: "Netflix",
            subtitle: "Month subscription",
            value: 12,
            color: kColorBlack,
            date: '07/05/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/paypal.png",
            title: "Paypal",
            subtitle: "Tax",
            value: 10,
            color: kColorDarkBlue,
            date: '05/05/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/paylater.png",
            title: "Paylater",
            subtitle: "Buy item",
            value: 2,
            color: kColorBlueSea,
            date: '03/05/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/paylater.png",
            title: "Paylater",
            subtitle: "Buy item",
            value: 2,
            color: kColorBlueSea,
            date: '03/05/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/paylater.png",
            title: "Paylater",
            subtitle: "Buy item",
            value: 2,
            color: kColorBlueSea,
            date: '30/04/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/paylater.png",
            title: "Paylater",
            subtitle: "Buy item",
            value: 2,
            color: kColorBlueSea,
            date: '27/04/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/netflix.png",
            title: "Netflix",
            subtitle: "Month subscription",
            value: 12,
            color: kColorBlack,
            date: '27/04/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/netflix.png",
            title: "Netflix",
            subtitle: "Month subscription",
            value: 12,
            color: kColorBlack,
            date: '25/04/2022',
            textValue: '\$ $value',
          ),
        );
        transactions.add(
          LastTransactionModel(
            icon: "assets/images/netflix.png",
            title: "Netflix",
            subtitle: "Month subscription",
            value: 12,
            color: kColorBlack,
            date: '20/04/2022',
            textValue: '\$ $value',
          ),
        );
        transaction.add(
          LastTransactionModel(
            icon: "assets/image/paypal.png",
            title: "Paypal",
            subtitle: "Tax",
            value: "\$10",
            color: kColorDarkBlue,
          ),
        );
        transaction.add(
          LastTransactionModel(
            icon: "assets/image/paypal.png",
            title: "Paypal",
            subtitle: "Tax",
            value: "\$10",
            color: kColorDarkBlue,
          ),
        );
        transaction.add(
          LastTransactionModel(
            icon: "assets/image/paypal.png",
            title: "Paypal",
            subtitle: "Tax",
            value: "\$10",
            color: kColorDarkBlue,
          ),
        );

        var now = DateTime.now();
        var beforeSevenDays =
            now.subtract(const Duration(days: 7)); // 29-04-2022
        var beforeThirtyDays =
            now.subtract(const Duration(days: 30)); // 29-04-2022
        var beforeSixtyDays =
            now.subtract(const Duration(days: 60)); // 29-04-2022
        var beforeNinetyDays =
            now.subtract(const Duration(days: 90)); // 29-04-2022
        var beforeOneHundredTwentyDays =
            now.subtract(const Duration(days: 120)); // 29-04-2022

        String? selectBeforeSevenDays() {
          for (var transaction in transactions) {
            if (transaction.toDate().isAfter(beforeSevenDays)) {
              return transaction.date;
            } else if (transaction.toDate().isAfter(beforeThirtyDays)) {
              return transaction.date;
            } else if (transaction.toDate().isAfter(beforeSixtyDays)) {
              return transaction.date;
            } else if (transaction.toDate().isAfter(beforeNinetyDays)) {
              return transaction.date;
            } else if (transaction
                .toDate()
                .isAfter(beforeOneHundredTwentyDays)) {
              return transaction.date;
            }
          }
        }

        return transactions;
      },
    );
  }
}
