import 'package:mabank/data/datasource/profile_datasource.dart';
import 'package:mabank/data/model/profile_model.dart';

class ProfileMockImpl implements ProfileDataSource {
  @override
  Future<ProfileModel> getProfile() {
    return Future.delayed(const Duration(seconds: 4)).then(
      (value) {
        return ProfileModel(
          image: 'assets/images/Profile.png',
          email: 'kaykycalhas@gmail.com',
          name: 'Roberto Gaules',
        );
      },
    );
  }
}
