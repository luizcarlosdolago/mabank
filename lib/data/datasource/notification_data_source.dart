import '../model/notification_model.dart';

abstract class NotificationDataSource {
  Future<List<NotificationModel>> getNotification();
}
