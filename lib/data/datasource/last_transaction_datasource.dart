import 'package:mabank/data/model/last_transaction_model.dart';

abstract class LastTransactionDataSource {
  Future<List<LastTransactionModel>> getlastTransaction();
}
