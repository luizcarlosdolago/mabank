class WalletModel {
  final String status;
  final String balance;
  final String brand;

  WalletModel(
      {required this.brand, required this.balance, required this.status});
}
