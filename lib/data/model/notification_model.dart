class NotificationModel {
  final String date;
  final bool wasRead;
  final String type;
  final String icon;
  final String message;
  NotificationModel(
      {required this.message,
      required this.icon,
      required this.date,
      required this.wasRead,
      required this.type});
}
