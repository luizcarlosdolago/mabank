class ProfileModel {
  final String image;
  final String name;
  final String email;

  ProfileModel({required this.email, required this.name, required this.image});
}
