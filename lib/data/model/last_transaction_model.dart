import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class LastTransactionModel {
  final String icon;
  final String title;
  final String subtitle;
  final double value;
  final String textValue;
  final Color color;
  final String date;

  LastTransactionModel(
      {required this.textValue,
      required this.date,
      required this.color,
      required this.icon,
      required this.title,
      required this.subtitle,
      required this.value});

  DateTime toDate() {
    var newFormat = DateFormat("dd/MM/yyyy");
    return newFormat.parse(date);
  }
}
