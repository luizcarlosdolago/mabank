import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  final double? size;
  const LoadingWidget({
    Key? key,
    required this.isLoading,
    required this.child,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? SizedBox(
            width: size,
            height: size,
            child: const CircularProgressIndicator(),
          )
        : child;
  }
}
