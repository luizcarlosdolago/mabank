import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/commons/styles.dart';
import 'package:mabank/data/datasource/wallet_data_source.dart';
import 'package:mabank/domain/repositories/wallet_repository.dart';
import 'package:mabank/domain/usecase/get_wallet_use_case.dart';
import 'package:mabank/features/components/wallet/wallet_component_controller.dart';

import '../../../data/datasource/mock/wallet_mock_impl.dart';
import '../../../data/repositories/wallet_repository_impl.dart';

class WalletComponet extends StatefulWidget {
  const WalletComponet({Key? key}) : super(key: key);

  @override
  State<WalletComponet> createState() => _WalletComponetState();
}

class _WalletComponetState extends State<WalletComponet> {
  late WalletRepository walletStatusRepository;
  WalletDataSource walletDataSource = WalletMockImpl();
  late GetWalletUseCase getWalletUsecase;
  late WalletRepository walletRepository;
  late GetWalletUseCase getWalletUseCase;
  late WalletComponentController _componentsController;

  @override
  void initState() {
    walletStatusRepository = WalletRepositoryImpl(walletDataSource);
    getWalletUsecase = GetWalletUseCase(walletStatusRepository);
    _componentsController = WalletComponentController(getWalletUsecase);
    walletRepository = WalletRepositoryImpl(walletDataSource);
    getWalletUseCase = GetWalletUseCase(walletRepository);
    _componentsController = WalletComponentController(getWalletUseCase);

    super.initState();
    _componentsController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Wallet',
          style: kHeadingLargeFont.copyWith(color: kColorDarkPurple1),
        ),
        Observer(
          builder: (context) {
            return _createWallet(_componentsController.state);
          },
        ),
      ],
    );
  }

  Widget _createWallet(AppState state) {
    if (state is LoadingAppState) {
      return _loadingAppState();
    } else {
      return _getStatus(_componentsController.state);
    }
  }

  Widget _loadingAppState() {
    return const Padding(
      padding: EdgeInsets.only(left: 30),
      child: SizedBox(
        width: 15,
        height: 15,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _getStatus(AppState state) {
    if (state is DataAppState) {
      return Text(
        state.data.status,
        style: GoogleFonts.quicksand(
          fontWeight: FontWeight.w600,
          fontSize: 16,
          letterSpacing: 0.5,
          color: kColorGray3,
        ),
      );
    }
    return Container();
  }
}
