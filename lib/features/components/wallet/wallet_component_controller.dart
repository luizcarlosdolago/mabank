import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/usecase/get_wallet_use_case.dart';
import 'package:mobx/mobx.dart';

import '../../../domain/entity/wallet_entity.dart';
part 'wallet_component_controller.g.dart';

class WalletComponentController = _WalletComponentControllerBase
    with _$WalletComponentController;

abstract class _WalletComponentControllerBase with Store {
  final GetWalletUseCase _getWalletUseCase;
  @observable
  AppState state = InitialAppState();

  _WalletComponentControllerBase(this._getWalletUseCase);

  @action
  void load() {
    state = LoadingAppState();
    _getWalletUseCase.call(NoParam()).then((value) {
      state = DataAppState<WalletEntity>(value);
    });
  }
}
