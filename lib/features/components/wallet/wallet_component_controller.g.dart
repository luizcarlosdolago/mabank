// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wallet_component_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$WalletComponentController on _WalletComponentControllerBase, Store {
  final _$stateAtom = Atom(name: '_WalletComponentControllerBase.state');

  @override
  AppState get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(AppState value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$_WalletComponentControllerBaseActionController =
      ActionController(name: '_WalletComponentControllerBase');

  @override
  void load() {
    final _$actionInfo = _$_WalletComponentControllerBaseActionController
        .startAction(name: '_WalletComponentControllerBase.load');
    try {
      return super.load();
    } finally {
      _$_WalletComponentControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
state: ${state}
    ''';
  }
}
