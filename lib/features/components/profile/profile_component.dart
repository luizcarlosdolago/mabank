import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/data/datasource/profile_datasource.dart';
import 'package:mabank/data/repositories/profile_repository_impl.dart';
import 'package:mabank/domain/entity/profile_entity.dart';
import 'package:mabank/domain/repositories/profile_repository.dart';
import 'package:mabank/domain/usecase/profile_get_use_case.dart';
import 'package:mabank/features/components/profile/profile_component_controller.dart';
import 'package:mabank/widgets/loading_widget.dart';

import '../../../data/datasource/mock/profile_mock_impl.dart';

class ProfileComponet extends StatefulWidget {
  const ProfileComponet({Key? key}) : super(key: key);

  @override
  State<ProfileComponet> createState() => _ProfileComponetState();
}

class _ProfileComponetState extends State<ProfileComponet> {
  late ProfileRepository profileRepository;
  ProfileDataSource profileDataSource = ProfileMockImpl();
  late ProfileGetUseCase profileGetUseCase;
  late ProfileComponentController _profileComponentController;

  @override
  void initState() {
    profileRepository = ProfileRepositoryImpl(profileDataSource);
    profileGetUseCase = ProfileGetUseCase(profileRepository);
    _profileComponentController = ProfileComponentController(profileGetUseCase);

    super.initState();
    _profileComponentController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Observer(
        builder: (_) {
          return LoadingWidget(
            isLoading: _profileComponentController.states is LoadingAppState,
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 4,
                    offset: Offset(2, 3), // Shadow position
                  ),
                ],
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.all(
                  Radius.circular(30),
                ),
                child: InkWell(
                  onTap: () {},
                  child: _getUrl(_profileComponentController.states),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _getUrl(AppState state) {
    if (state is DataAppState<ProfileEntity>) {
      return Image.asset(state.data.image);
    }
    return Container();
  }
}
