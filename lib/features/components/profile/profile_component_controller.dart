import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/profile_entity.dart';
import 'package:mabank/domain/usecase/profile_get_use_case.dart';
import 'package:mobx/mobx.dart';
part 'profile_component_controller.g.dart';

class ProfileComponentController = _ProfileComponentControllerBase
    with _$ProfileComponentController;

abstract class _ProfileComponentControllerBase with Store {
  final ProfileGetUseCase _profileGetUseCase;

  _ProfileComponentControllerBase(this._profileGetUseCase);

  @observable
  AppState states = InitialAppState();

  @action
  void load() {
    states = LoadingAppState();

    _profileGetUseCase.call(NoParam()).then((value) {
      states = DataAppState<ProfileEntity>(value);
    });
  }
}
