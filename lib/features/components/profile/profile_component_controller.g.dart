// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_component_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ProfileComponentController on _ProfileComponentControllerBase, Store {
  final _$statesAtom = Atom(name: '_ProfileComponentControllerBase.states');

  @override
  AppState get states {
    _$statesAtom.reportRead();
    return super.states;
  }

  @override
  set states(AppState value) {
    _$statesAtom.reportWrite(value, super.states, () {
      super.states = value;
    });
  }

  final _$_ProfileComponentControllerBaseActionController =
      ActionController(name: '_ProfileComponentControllerBase');

  @override
  void load() {
    final _$actionInfo = _$_ProfileComponentControllerBaseActionController
        .startAction(name: '_ProfileComponentControllerBase.load');
    try {
      return super.load();
    } finally {
      _$_ProfileComponentControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
states: ${states}
    ''';
  }
}
