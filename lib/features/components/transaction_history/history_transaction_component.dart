import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/colors.dart';

import '../../../data/datasource/last_transaction_datasource.dart';
import '../../../data/datasource/mock/last_transaction_mock_impl.dart';
import '../../../data/repositories/last_transaction_repository_impl.dart';
import '../../../domain/entity/last_transaction_entity.dart';
import '../../../domain/repositories/last_transaction_repository.dart';
import '../../../domain/usecase/last_transaction_get_use_case.dart';
import 'last_transaction_components_controller.dart';

class HistoryTransactionComponent extends StatefulWidget {
  const HistoryTransactionComponent({Key? key}) : super(key: key);

  @override
  State<HistoryTransactionComponent> createState() =>
      _HistoryTransactionComponentState();
}

class _HistoryTransactionComponentState
    extends State<HistoryTransactionComponent> {
  late LastTransactionRepository lastTransactionRepository;
  LastTransactionDataSource lastTransactionDataSource =
      LastTransactionMockImpl();
  late LastTransactionGetUseCase lastTransactionGetUseCase;
  late LastTransactionComponentsController _lastTransactionComponentsController;

  @override
  void initState() {
    lastTransactionRepository =
        LastTRansactionRepsitoryImpl(lastTransactionDataSource);
    lastTransactionGetUseCase =
        LastTransactionGetUseCase(lastTransactionRepository);
    _lastTransactionComponentsController =
        LastTransactionComponentsController(lastTransactionGetUseCase);
    super.initState();

    _lastTransactionComponentsController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "Transaction History",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: kColorDarkPurple1,
                ),
              ),
            ],
          ),
          Observer(
            builder: (_) {
              return _createHistoryTransaction(
                  _lastTransactionComponentsController.states);
            },
          )
        ],
      ),
    );
  }

  Widget _createHistoryTransaction(AppState state) {
    if (state is LoadingAppState) {
      return _loadingAppState();
    } else {
      return _getHistoryTransaction(
          _lastTransactionComponentsController.states);
    }
  }

  Widget _loadingAppState() {
    return const SizedBox(
      height: 250,
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _getHistoryTransaction(AppState state) {
    if (state is DataAppState<List<LastTransactionEntity>>) {
      List<Widget> children = [];
      for (var index = 0; index < state.data.length; index++ + 1) {
        var data = state.data[index];
        children.add(
          Row(
            children: [
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage(data.icon),
                  ),
                  color: data.color,
                ),
                width: 44,
                height: 44,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.title,
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: kColorDarkPurple1,
                      ),
                    ),
                    Text(
                      data.subtitle,
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w500,
                        fontSize: 13,
                        letterSpacing: 0.5,
                        color: kColorGray3,
                      ),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Text(
                  data.value.toString(),
                  style: GoogleFonts.rubik(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: kColorDarkPurple,
                  ),
                ),
              ),
            ],
          ),
        );
        children.add(
          const SizedBox(
            height: 20,
          ),
        );
      }
      children.add(
        const SizedBox(
          height: 85,
        ),
      );

      return Expanded(
        child: ListView(
          children: children,
        ),
      );
    }
    return Container();
  }
}
