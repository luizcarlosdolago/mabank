import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/data/datasource/mock/last_transaction_mock_impl.dart';
import 'package:mabank/domain/entity/last_transaction_entity.dart';
import 'package:mabank/domain/usecase/last_transaction_get_use_case.dart';
import 'package:mabank/features/components/grafic_stats/drop_down_button_widget.dart';
import 'package:mobx/mobx.dart';
part 'last_transaction_components_controller.g.dart';

class LastTransactionComponentsController = _LastTransactionComponentsControllerBase
    with _$LastTransactionComponentsController;

abstract class _LastTransactionComponentsControllerBase with Store {
  final LastTransactionGetUseCase _lastTransactionGetUseCase;

  _LastTransactionComponentsControllerBase(this._lastTransactionGetUseCase);

  @observable
  AppState states = InitialAppState();

  @action
  void load() {
    states = LoadingAppState();
    _lastTransactionGetUseCase.call(NoParam()).then((value) {
      states = DataAppState<List<LastTransactionEntity>>(value);
    });
  }
}
