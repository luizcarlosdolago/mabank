import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/commons/styles.dart';
import 'package:mabank/data/datasource/mock/wallet_mock_impl.dart';
import 'package:mabank/data/datasource/wallet_data_source.dart';
import 'package:mabank/data/repositories/wallet_repository_impl.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/repositories/wallet_repository.dart';
import 'package:mabank/domain/usecase/get_wallet_use_case.dart';
import 'package:mabank/widgets/loading_widget.dart';

import 'info_section_component_controller.dart';

class InfoSectionComponent extends StatefulWidget {
  const InfoSectionComponent({Key? key}) : super(key: key);

  @override
  State<InfoSectionComponent> createState() => _InfoSectionComponentState();
}

class _InfoSectionComponentState extends State<InfoSectionComponent> {
  late WalletRepository walletStatusRepository;
  WalletDataSource walletDataSource = WalletMockImpl();
  late GetWalletUseCase getWalletUsecase;
  late WalletRepository walletRepository;
  late GetWalletUseCase getWalletUseCase;
  late InfoSectionComponentController _infoSectionComponentController;

  @override
  void initState() {
    walletStatusRepository = WalletRepositoryImpl(walletDataSource);
    getWalletUsecase = GetWalletUseCase(walletStatusRepository);
    _infoSectionComponentController =
        InfoSectionComponentController(getWalletUsecase);
    walletRepository = WalletRepositoryImpl(walletDataSource);
    getWalletUseCase = GetWalletUseCase(walletRepository);
    _infoSectionComponentController =
        InfoSectionComponentController(getWalletUseCase);

    super.initState();
    _infoSectionComponentController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 140.0,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(30.0),
            child: Stack(
              children: [
                Container(
                  height: 140,
                  width: MediaQuery.of(context).size.width,
                  color: kColorInfoSection,
                ),
                Positioned(
                  top: -27,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.81),
                    child: Container(
                      height: 54,
                      width: 58.35,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xD06E34B8),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -70,
                  left: MediaQuery.of(context).size.width - 150,
                  child: Container(
                    height: 143.0,
                    width: 154.53,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: kColorDarkPurple,
                        width: 3.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Balance",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0,
                        color: kColorWhite,
                        letterSpacing: 0.36,
                      ),
                    ),
                    Observer(builder: (context) {
                      String value = "--";
                      if (_infoSectionComponentController.states
                          is DataAppState<WalletEntity>) {
                        DataAppState<WalletEntity> state =
                            _infoSectionComponentController.states
                                as DataAppState<WalletEntity>;
                        value = state.data.balance;
                      }
                      return LoadingWidget(
                        size: 15,
                        isLoading: _infoSectionComponentController.states
                            is LoadingAppState,
                        child: Text(
                          value,
                          style: kTextInfoSection.copyWith(color: kColorWhite),
                        ),
                      );
                    }),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Brand",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0,
                        color: kColorWhite,
                        letterSpacing: 0.36,
                      ),
                    ),
                    Observer(
                      builder: (context) {
                        String value = "--";
                        if (_infoSectionComponentController.states
                            is DataAppState<WalletEntity>) {
                          DataAppState<WalletEntity> state =
                              _infoSectionComponentController.states
                                  as DataAppState<WalletEntity>;
                          value = state.data.brand;
                        }
                        return LoadingWidget(
                          size: 15,
                          isLoading: _infoSectionComponentController.states
                              is LoadingAppState,
                          child: Text(
                            value,
                            style:
                                kTextInfoSection.copyWith(color: kColorWhite),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
