import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/usecase/get_wallet_use_case.dart';
import 'package:mobx/mobx.dart';
part 'info_section_component_controller.g.dart';

class InfoSectionComponentController = _InfoSectionComponentControllerBase
    with _$InfoSectionComponentController;

abstract class _InfoSectionComponentControllerBase with Store {
  final GetWalletUseCase _getWalletUseCase;
  @observable
  AppState states = InitialAppState();

  _InfoSectionComponentControllerBase(this._getWalletUseCase);

  @action
  void load() {
    states = LoadingAppState();
    _getWalletUseCase.call(NoParam()).then((value) {
      states = DataAppState<WalletEntity>(value);
    });
  }
}
