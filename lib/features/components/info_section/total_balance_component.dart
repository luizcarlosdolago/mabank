import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/data/datasource/mock/wallet_mock_impl.dart';
import 'package:mabank/data/datasource/wallet_data_source.dart';
import 'package:mabank/data/repositories/wallet_repository_impl.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/repositories/wallet_repository.dart';
import 'package:mabank/domain/usecase/get_wallet_use_case.dart';

import '../../../commons/colors.dart';
import 'info_section_component_controller.dart';

class TotalBalanceComponent extends StatefulWidget {
  const TotalBalanceComponent({Key? key}) : super(key: key);

  @override
  State<TotalBalanceComponent> createState() => _TotalBalanceComponentState();
}

class _TotalBalanceComponentState extends State<TotalBalanceComponent> {
  late WalletRepository walletRepository;
  WalletDataSource walletDataSource = WalletMockImpl();
  late GetWalletUseCase getWalletUseCase;
  late InfoSectionComponentController _infoSectionComponentController;

  @override
  void initState() {
    walletRepository = WalletRepositoryImpl(walletDataSource);
    getWalletUseCase = GetWalletUseCase(walletRepository);
    _infoSectionComponentController =
        InfoSectionComponentController(getWalletUseCase);
    super.initState();

    _infoSectionComponentController.load();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 102,
      child: Column(
        children: [
          SizedBox(
            height: 24,
            child: Text(
              "Total Balance",
              style: GoogleFonts.quicksand(
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: kColorDarkGray,
              ),
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          SizedBox(
            width: 143,
            height: 54,
            child: Observer(
              builder: (_) {
                return Center(
                  child: _createTotalBalance(
                      _infoSectionComponentController.states),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _createTotalBalance(AppState state) {
    if (state is LoadingAppState) {
      return _loadingAppState();
    } else {
      return _totalBalance(_infoSectionComponentController.states);
    }
  }

  Widget _loadingAppState() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _totalBalance(AppState state) {
    if (state is DataAppState<WalletEntity>) {
      return Text(
        state.data.balance,
        style: GoogleFonts.poppins(
          fontWeight: FontWeight.w500,
          fontSize: 36,
          color: kColorPurple,
        ),
      );
    }
    return Container();
  }
}
