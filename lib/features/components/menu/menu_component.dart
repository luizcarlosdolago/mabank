// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/colors.dart';

class MenuComponent extends StatefulWidget {
  final VoidCallback onPressed;
  final String image;
  final IconData icon;
  final String text;
  const MenuComponent(
      {Key? key,
      required this.icon,
      required this.text,
      required this.image,
      required this.onPressed})
      : super(key: key);

  @override
  State<MenuComponent> createState() => _MenuComponentState();
}

class _MenuComponentState extends State<MenuComponent> {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      height: 54,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        side: BorderSide(color: kColorGray6),
      ),
      onPressed: widget.onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(widget.image),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              widget.text,
              style: GoogleFonts.quicksand(
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: kColorDarkPurple1,
              ),
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.only(right: 25),
            child: Icon(widget.icon, size: 10),
          ),
        ],
      ),
    );
  }
}
