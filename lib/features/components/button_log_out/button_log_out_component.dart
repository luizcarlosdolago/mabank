import 'package:flutter/material.dart';
import 'package:mabank/commons/colors.dart';

class ButtonLogOutComponent extends StatefulWidget {
  final GestureTapCallback onTap;
  const ButtonLogOutComponent({Key? key, required this.onTap})
      : super(key: key);

  @override
  State<ButtonLogOutComponent> createState() => _ButtonLogOutComponentState();
}

class _ButtonLogOutComponentState extends State<ButtonLogOutComponent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: kColorGray6),
              borderRadius: BorderRadius.circular(50),
            ),
            width: 64,
            height: 64,
            child: Image.asset("assets/icon/login.png"),
          ),
          const SizedBox(
            width: 70,
            height: 32,
            child: Center(
              child: Text(
                "Log Out",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: Color(0xff9038FF),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
