import 'package:flutter/material.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/commons/styles.dart';
import 'package:mabank/data/datasource/last_transaction_datasource.dart';
import 'package:mabank/data/datasource/mock/last_transaction_mock_impl.dart';
import 'package:mabank/data/repositories/last_transaction_repository_impl.dart';
import 'package:mabank/domain/repositories/last_transaction_repository.dart';
import 'package:mabank/domain/usecase/last_transaction_get_use_case.dart';
import 'package:mabank/features/components/grafic_stats/drop_down_button_widget.dart';
import 'package:mabank/features/components/transaction_history/last_transaction_components_controller.dart';

class GraficStatsComponent extends StatefulWidget {
  const GraficStatsComponent({Key? key}) : super(key: key);

  @override
  State<GraficStatsComponent> createState() => _GraficStatsComponentState();
}

class _GraficStatsComponentState extends State<GraficStatsComponent> {
  late LastTransactionRepository lastTransactionRepository;
  late LastTransactionDataSource lastTransactionDataSource =
      LastTransactionMockImpl();
  late LastTransactionGetUseCase lastTransactionGetUseCase;
  late LastTransactionComponentsController _lastTransactionComponentsController;

  @override
  void initState() {
    lastTransactionRepository =
        LastTRansactionRepsitoryImpl(lastTransactionDataSource);
    lastTransactionGetUseCase =
        LastTransactionGetUseCase(lastTransactionRepository);
    _lastTransactionComponentsController =
        LastTransactionComponentsController(lastTransactionGetUseCase);
    super.initState();

    _lastTransactionComponentsController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 232.0,
        child: Column(
          children: [
            incomeStats(),
            const SizedBox(
              height: 20.0,
            ),
            // graficStats(),
          ],
        ),
      ),
    );
  }

  // Widget graficStats() {
  //   return SizedBox(
  //     height: 170,
  //     child:
  //   );
  // }

  Widget incomeStats() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 32.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 32.0,
            child: Expanded(
              child: Text(
                "Income Stats",
                style:
                    kTextHeadingMediumFont.copyWith(color: kColorDarkPurple1),
              ),
            ),
          ),
          const SizedBox(
            child: DropDownButtonWidget(),
          ),
        ],
      ),
    );
  }
}
