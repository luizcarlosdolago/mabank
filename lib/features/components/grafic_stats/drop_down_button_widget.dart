import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/commons/styles.dart';

class DropDownButtonWidget extends StatefulWidget {
  final String? selectedDays;
  const DropDownButtonWidget({Key? key, this.selectedDays}) : super(key: key);

  @override
  State<DropDownButtonWidget> createState() => _DropDownButtonWidgetState();
}

class _DropDownButtonWidgetState extends State<DropDownButtonWidget> {
  List<String> days = ['7 dias', '30 dias', '60 dias', '90 dias', '120 dias'];
  String? selectedDays = '7 dias';

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButton<String>(
        icon: SvgPicture.asset(
          'assets/images/arrow_left.svg',
          width: 9.43,
          height: 11.25,
        ),
        value: selectedDays,
        items: days
            .map(
              (days) => DropdownMenuItem<String>(
                value: days,
                child: Text(
                  days,
                  style: kTextQuicksandMediumFont.copyWith(
                      color: kColorDarkestGray),
                ),
              ),
            )
            .toList(),
        onChanged: (days) => setState(() => selectedDays = days),
      ),
    );
  }
}
