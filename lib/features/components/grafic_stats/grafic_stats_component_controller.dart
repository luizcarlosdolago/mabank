import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/last_transaction_entity.dart';
import 'package:mabank/domain/usecase/last_transaction_get_use_case.dart';
import 'package:mobx/mobx.dart';
part 'grafic_stats_component_controller.g.dart';

class GraficStatsComponentController = _GraficStatsComponentControllerBase
    with _$GraficStatsComponentController;

abstract class _GraficStatsComponentControllerBase with Store {
  final LastTransactionGetUseCase _getUseCase;

  _GraficStatsComponentControllerBase(this._getUseCase);

  @observable
  AppState states = InitialAppState();

  @action
  void load() {
    states = LoadingAppState();
    _getUseCase.call(NoParam()).then((value) {
      states = DataAppState<List<LastTransactionEntity>>(value);
    });
  }
}
