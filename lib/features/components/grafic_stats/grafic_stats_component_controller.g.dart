// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'grafic_stats_component_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GraficStatsComponentController
    on _GraficStatsComponentControllerBase, Store {
  final _$statesAtom = Atom(name: '_GraficStatsComponentControllerBase.states');

  @override
  AppState get states {
    _$statesAtom.reportRead();
    return super.states;
  }

  @override
  set states(AppState value) {
    _$statesAtom.reportWrite(value, super.states, () {
      super.states = value;
    });
  }

  final _$_GraficStatsComponentControllerBaseActionController =
      ActionController(name: '_GraficStatsComponentControllerBase');

  @override
  void load() {
    final _$actionInfo = _$_GraficStatsComponentControllerBaseActionController
        .startAction(name: '_GraficStatsComponentControllerBase.load');
    try {
      return super.load();
    } finally {
      _$_GraficStatsComponentControllerBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
states: ${states}
    ''';
  }
}
