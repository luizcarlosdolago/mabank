import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/commons/styles.dart';

class FeaturedSectionComponent extends StatelessWidget {
  final String title;
  final String icon;
  const FeaturedSectionComponent(
      {Key? key, required this.title, required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 58.0,
      child: TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
        ),
        onPressed: () {},
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                color: kColorWhite,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 3.0),
                      blurRadius: 6.0)
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: SvgPicture.asset(
                  icon,
                  width: 28.0,
                  height: 28.0,
                ),
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: kQuicksandFont.copyWith(color: kColorGrey2),
            )
          ],
        ),
      ),
    );
  }
}
