import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/data/datasource/mock/notification_mock_impl.dart';
import 'package:mabank/data/datasource/notification_data_source.dart';
import 'package:mabank/data/repositories/notification_repository_impl.dart';
import 'package:mabank/domain/entity/notification_entity.dart';
import 'package:mabank/domain/repositories/notification_repository.dart';
import 'package:mabank/domain/usecase/get_notification_use_case_impl.dart';
import 'package:mabank/features/components/list_notification/notification_controller_component_widget.dart';

class ListNotificationCoponent extends StatefulWidget {
  const ListNotificationCoponent({Key? key}) : super(key: key);

  @override
  State<ListNotificationCoponent> createState() =>
      _ListNotificationCoponentState();
}

class _ListNotificationCoponentState extends State<ListNotificationCoponent> {
  late NotificationRepository notificationRepository;
  NotificationDataSource notificationDataSource = NotificationMockImpl();
  late NotificationUseCaseImpl notificationUseCaseImpl;
  late NotificationControllerComponent _notificationControllerComponent;

  @override
  void initState() {
    notificationRepository = NotificationREpositoryImpl(notificationDataSource);
    notificationUseCaseImpl = NotificationUseCaseImpl(notificationRepository);
    _notificationControllerComponent =
        NotificationControllerComponent(notificationUseCaseImpl);

    super.initState();
    _notificationControllerComponent.load();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return _createNotification(_notificationControllerComponent.states);
      },
    );
  }

  Widget _createNotification(AppState state) {
    if (state is LoadingAppState) {
      return _loadingAppState();
    } else {
      return _listNotification(_notificationControllerComponent.states);
    }
  }

  Widget _loadingAppState() {
    return const SizedBox(
      height: 500,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _listNotification(AppState state) {
    String section = '';
    if (state is DataAppState<List<NotificationEntity>>) {
      List<Widget> children = [];
      for (var index = 0; index < state.data.length; index++) {
        var item = state.data[index];
        String currentSection = item.wasRead ? 'Recent' : 'New';
        if (section != currentSection) {
          if (section != '') {
            children.add(
              const SizedBox(
                height: 20,
              ),
            );
          }
          section = currentSection;
          children.add(
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                currentSection,
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: kColorDarkPurple1,
                ),
              ),
            ),
          );
          children.add(
            const SizedBox(
              height: 20,
            ),
          );
        }
        children.add(
          Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 90,
              child: Stack(
                children: [
                  Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width,
                    height: 90,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20, top: 10, bottom: 8, right: 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 72,
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              item.date,
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w400,
                                fontSize: 11,
                                color: kColorGray3,
                              ),
                            ),
                            Text(
                              item.message,
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w500,
                                fontSize: 13,
                                letterSpacing: 0.5,
                                color: kColorGelap,
                              ),
                            ),
                            Text(
                              item.type,
                              style: GoogleFonts.quicksand(
                                fontWeight: FontWeight.w400,
                                fontSize: 11,
                                color: kColorGray3,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 20, top: 12),
                      child: Image.asset(item.icon),
                    ),
                  ),
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            elevation: 5,
          ),
        );
      }
      children.add(
        const SizedBox(
          height: 85,
        ),
      );

      return Expanded(
        child: ListView(
          children: children,
        ),
      );
    }
    return Container();
  }
}
