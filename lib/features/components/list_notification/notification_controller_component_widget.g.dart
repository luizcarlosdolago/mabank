// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_controller_component_widget.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NotificationControllerComponent
    on _NotificationControllerComponentBase, Store {
  final _$statesAtom =
      Atom(name: '_NotificationControllerComponentBase.states');

  @override
  AppState get states {
    _$statesAtom.reportRead();
    return super.states;
  }

  @override
  set states(AppState value) {
    _$statesAtom.reportWrite(value, super.states, () {
      super.states = value;
    });
  }

  final _$_NotificationControllerComponentBaseActionController =
      ActionController(name: '_NotificationControllerComponentBase');

  @override
  void load() {
    final _$actionInfo = _$_NotificationControllerComponentBaseActionController
        .startAction(name: '_NotificationControllerComponentBase.load');
    try {
      return super.load();
    } finally {
      _$_NotificationControllerComponentBaseActionController
          .endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
states: ${states}
    ''';
  }
}
