import 'package:mabank/commons/app_states.dart';
import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/usecase/get_notification_use_case_impl.dart';
import 'package:mobx/mobx.dart';

import '../../../domain/entity/notification_entity.dart';
part 'notification_controller_component_widget.g.dart';

class NotificationControllerComponent = _NotificationControllerComponentBase
    with _$NotificationControllerComponent;

abstract class _NotificationControllerComponentBase with Store {
  final NotificationUseCaseImpl _notificationUseCaseImpl;

  _NotificationControllerComponentBase(this._notificationUseCaseImpl);

  @observable
  AppState states = InitialAppState();

  @action
  void load() {
    states = LoadingAppState();
    _notificationUseCaseImpl.call(NoParam()).then((value) {
      states = DataAppState<List<NotificationEntity>>(value);
    });
  }
}
