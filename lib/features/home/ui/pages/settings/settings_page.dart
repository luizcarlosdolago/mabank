import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/features/components/button_log_out/button_log_out_component.dart';
import 'package:mabank/features/components/menu/menu_component.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SizedBox(
        height: MediaQuery.of(context).size.height - 42,
        child: Column(
          children: [
            const SizedBox(
              height: 70,
            ),
            Text(
              "Settings",
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: kColorDarkPurple3,
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            MenuComponent(
              image: 'assets/icon/profile.png',
              text: 'Profile',
              icon: Icons.arrow_forward_ios_outlined,
              onPressed: () {
                Navigator.pushNamed(context, "/profileScreen");
              },
            ),
            const SizedBox(
              height: 10,
            ),
            MenuComponent(
              image: 'assets/icon/notification.png',
              text: 'Notifications',
              icon: Icons.arrow_forward_ios_outlined,
              onPressed: () {
                debugPrint("clicou");
              },
            ),
            const SizedBox(
              height: 10,
            ),
            MenuComponent(
              image: "assets/icon/wallet-2.png",
              text: 'Your Wallet',
              icon: Icons.arrow_forward_ios_outlined,
              onPressed: () {
                debugPrint("clicou");
              },
            ),
            const SizedBox(
              height: 10,
            ),
            MenuComponent(
              image: 'assets/icon/key-square.png',
              text: 'Login Settings',
              icon: Icons.arrow_forward_ios_outlined,
              onPressed: () {
                debugPrint("clicou");
              },
            ),
            const SizedBox(
              height: 10,
            ),
            MenuComponent(
              image: 'assets/icon/call-calling.png',
              text: 'Service Center',
              icon: Icons.arrow_forward_ios_outlined,
              onPressed: () {
                debugPrint("clicou");
              },
            ),
            const Spacer(),
            ButtonLogOutComponent(
              onTap: () {
                debugPrint("Clicou");
              },
            ),
            const SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
