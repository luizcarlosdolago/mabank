import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/features/components/list_notification/list_notification_component.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SizedBox(
        height: MediaQuery.of(context).size.height - 42,
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Text(
              "Notifications",
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: kColorDarkPurple3,
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const ListNotificationCoponent(),
          ],
        ),
      ),
    );
  }
}
