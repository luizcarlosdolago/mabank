import 'package:flutter/material.dart';
import 'package:mabank/features/components/featured_section/featured_section_component.dart';
import 'package:mabank/features/components/info_section/info_section_component.dart';
import 'package:mabank/features/components/wallet/wallet_component.dart';

import '../../../../components/profile/profile_component.dart';
import '../../../../components/transaction_history/last_transaction_componenent.dart';

class WalletPage extends StatefulWidget {
  const WalletPage({Key? key}) : super(key: key);

  @override
  State<WalletPage> createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SizedBox(
        height: MediaQuery.of(context).size.height - 42,
        child: Column(
          children: [
            const SizedBox(
              height: 69,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                WalletComponet(),
                SizedBox(
                  width: 64,
                  child: ProfileComponet(),
                ),
              ],
            ),
            const SizedBox(
              height: 40.0,
            ),
            const InfoSectionComponent(),
            const SizedBox(
              height: 40.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                FeaturedSectionComponent(
                    title: "Transfer", icon: "assets/images/convert.svg"),
                FeaturedSectionComponent(
                    title: "Payment", icon: "assets/images/export.svg"),
                FeaturedSectionComponent(
                    title: "Payout", icon: "assets/images/money-send.svg"),
                FeaturedSectionComponent(
                    title: "Top Up", icon: "assets/images/add-circle.svg"),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            const LastTransaction(),
          ],
        ),
      ),
    );
  }
}
