import 'package:flutter/material.dart';
import 'package:mabank/features/components/grafic_stats/grafic_stats_component.dart';
import 'package:mabank/features/components/info_section/total_balance_component.dart';
import 'package:mabank/features/components/transaction_history/history_transaction_component.dart';

class StatsPage extends StatefulWidget {
  const StatsPage({Key? key}) : super(key: key);

  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: SizedBox(
        height: MediaQuery.of(context).size.height - 42,
        child: Column(
          children: const [
            TotalBalanceComponent(),
            SizedBox(
              height: 78,
            ),
            GraficStatsComponent(),
            SizedBox(
              height: 40,
            ),
            HistoryTransactionComponent()
          ],
        ),
      ),
    );
  }
}
