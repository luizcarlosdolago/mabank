import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mabank/features/home/ui/pages/settings/settings_page.dart';
import 'package:mabank/features/home/ui/pages/stats/stats_page.dart';
import 'package:mabank/features/home/ui/pages/wallet/wallet_page.dart';
import '../../../commons/bottom_navigation_bar_widget.dart';
import 'pages/notification/notification_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _tabs = [
    const WalletPage(),
    const StatsPage(),
    const NotificationPage(),
    const SettingsPage(),
  ];
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate(
                    [_tabs[selectedIndex]],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, bottom: 27, right: 20),
                child: _bottomNavigationBarWidget(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottomNavigationBarWidget() {
    return BottomNavigationBarWidget(
      items: [
        BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/images/Vector.svg")),
        BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/images/chart-2.svg")),
        BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/images/notification.svg")),
        BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/images/setting.svg")),
      ],
      ontap: (idx) {
        setState(
          () {
            selectedIndex = idx;
          },
        );
      },
    );
  }
}
