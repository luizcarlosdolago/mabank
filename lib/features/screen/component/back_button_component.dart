// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class BackButtonComponent extends StatefulWidget {
  final VoidCallback onPressed;
  const BackButtonComponent({Key? key, required this.onPressed})
      : super(key: key);

  @override
  State<BackButtonComponent> createState() => _BackButtonComponentState();
}

class _BackButtonComponentState extends State<BackButtonComponent> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: FlatButton(
        minWidth: 44,
        height: 44,
        shape: const CircleBorder(),
        onPressed: widget.onPressed,
        child: Image.asset(
          "assets/icon/Back Button.png",
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
