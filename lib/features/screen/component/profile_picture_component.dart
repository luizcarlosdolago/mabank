import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/app_states.dart';
import 'package:mabank/data/datasource/mock/profile_mock_impl.dart';
import 'package:mabank/data/datasource/profile_datasource.dart';
import 'package:mabank/data/repositories/profile_repository_impl.dart';
import 'package:mabank/domain/entity/profile_entity.dart';
import 'package:mabank/domain/repositories/profile_repository.dart';
import 'package:mabank/domain/usecase/profile_get_use_case.dart';
import 'package:mabank/features/components/profile/profile_component_controller.dart';

class ProfilePictureComponet extends StatefulWidget {
  const ProfilePictureComponet({Key? key}) : super(key: key);

  @override
  State<ProfilePictureComponet> createState() => _ProfilePictureComponetState();
}

class _ProfilePictureComponetState extends State<ProfilePictureComponet> {
  late ProfileRepository profileRepository;
  ProfileDataSource profileDataSource = ProfileMockImpl();
  late ProfileGetUseCase profileGetUseCase;
  late ProfileComponentController _profileComponentController;

  @override
  void initState() {
    profileRepository = ProfileRepositoryImpl(profileDataSource);
    profileGetUseCase = ProfileGetUseCase(profileRepository);
    _profileComponentController = ProfileComponentController(profileGetUseCase);
    super.initState();
    _profileComponentController.load();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 106, right: 105),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Observer(
              builder: (_) {
                return _createImageProfile(_profileComponentController.states);
              },
            ),
            const SizedBox(
              height: 10,
            ),
            SizedBox(
              child: Observer(
                builder: (_) {
                  return _createNameProfile(_profileComponentController.states);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _createImageProfile(AppState state) {
    if (state is LoadingAppState) {
      return _createLadingAppState();
    } else {
      return _getImageProfile(_profileComponentController.states);
    }
  }

  Widget _createLadingAppState() {
    return const Padding(
      padding: EdgeInsets.only(top: 40),
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _getImageProfile(AppState state) {
    if (state is DataAppState<ProfileEntity>) {
      return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
        ),
        width: 72,
        height: 72,
        child: Image.asset(
          state.data.image,
          fit: BoxFit.fitHeight,
        ),
      );
    }
    return Container();
  }

  Widget _createNameProfile(AppState state) {
    if (state is DataAppState) {
      return _getNameProfile(_profileComponentController.states);
    }
    return Container();
  }

  Widget _getNameProfile(AppState state) {
    if (state is DataAppState<ProfileEntity>) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Text(
              state.data.email,
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                fontSize: 18,
              ),
            ),
          ),
          const SizedBox(
            width: 12.5,
          ),
          SizedBox(
            child: Image.asset("assets/icon/edit-2.png"),
          ),
        ],
      );
    }
    return Container();
  }
}
