import 'package:flutter/material.dart';
import 'package:mabank/commons/colors.dart';

class ButtonDeleteAccountComponent extends StatefulWidget {
  final GestureTapCallback onTap;
  const ButtonDeleteAccountComponent({Key? key, required this.onTap})
      : super(key: key);

  @override
  State<ButtonDeleteAccountComponent> createState() =>
      _ButtonDeleteAccountComponentState();
}

class _ButtonDeleteAccountComponentState
    extends State<ButtonDeleteAccountComponent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(color: kColorGray5),
            ),
            width: 64,
            height: 64,
            child: Image.asset("assets/icon/trash.png"),
          ),
          const SizedBox(
            width: 142,
            height: 42,
            child: Center(
              child: Text(
                "Delete Account",
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: Color(0xff9038FF),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
