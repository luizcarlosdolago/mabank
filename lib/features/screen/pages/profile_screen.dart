// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mabank/commons/colors.dart';
import 'package:mabank/features/screen/component/back_button_component.dart';
import 'package:mabank/features/screen/component/button_delete_acconout_component.dart';
import 'package:mabank/features/screen/component/profile_picture_component.dart';

import '../../components/menu/menu_component.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              BackButtonComponent(
                onPressed: () {
                  // ignore: avoid_print
                  debugPrint("Clicou");
                  Navigator.pop(context);
                },
              ),
              const SizedBox(
                height: 50,
              ),
              Text(
                "Profile",
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: kColorDarkPurple3,
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              const ProfilePictureComponet(),
              const SizedBox(
                height: 40,
              ),
              MenuComponent(
                image: 'assets/icon/link.png',
                text: 'Connected Account',
                icon: Icons.arrow_forward_ios_outlined,
                onPressed: () {
                  debugPrint("clicou");
                },
              ),
              const SizedBox(
                height: 10,
              ),
              MenuComponent(
                image: 'assets/icon/security-safe.png',
                text: 'Privacy and Security',
                icon: Icons.arrow_forward_ios_outlined,
                onPressed: () {
                  debugPrint("clicou");
                },
              ),
              const SizedBox(
                height: 10,
              ),
              MenuComponent(
                image: 'assets/icon/key.png',
                text: 'Login Settings',
                icon: Icons.arrow_forward_ios_outlined,
                onPressed: () {
                  debugPrint("clicou");
                },
              ),
              const SizedBox(
                height: 10,
              ),
              MenuComponent(
                image: 'assets/icon/message-question.png',
                text: 'Service Center',
                icon: Icons.arrow_forward_ios_outlined,
                onPressed: () {
                  debugPrint("clicou");
                },
              ),
              const Spacer(),
              ButtonDeleteAccountComponent(
                onTap: () {
                  debugPrint("clicou");
                },
              ),
              const SizedBox(
                height: 28,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
