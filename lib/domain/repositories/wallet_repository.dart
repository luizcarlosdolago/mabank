import 'package:mabank/domain/entity/wallet_entity.dart';

abstract class WalletRepository {
  Future<WalletEntity> getWallet();
}
