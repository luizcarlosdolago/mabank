import 'package:mabank/domain/entity/last_transaction_entity.dart';

abstract class LastTransactionRepository {
  Future<List<LastTransactionEntity>> getLastTRansaction();
}