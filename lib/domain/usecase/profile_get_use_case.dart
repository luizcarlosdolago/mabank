import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/profile_entity.dart';
import 'package:mabank/domain/repositories/profile_repository.dart';

class ProfileGetUseCase implements UseCase<ProfileEntity,NoParam> {
  final ProfileRepository _profileRepository;

  ProfileGetUseCase(this._profileRepository);
  @override
  Future<ProfileEntity> call(NoParam param) {
    return _profileRepository.getProfile();
  }

}