import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/repositories/notification_repository.dart';

import '../entity/notification_entity.dart';

class NotificationUseCaseImpl
    implements UseCase<List<NotificationEntity>, NoParam> {
  final NotificationRepository _notificationRepository;

  NotificationUseCaseImpl(this._notificationRepository);
  @override
  Future<List<NotificationEntity>> call(NoParam param) {
    return _notificationRepository.getNotification();
  }
}
