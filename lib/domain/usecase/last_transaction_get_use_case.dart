import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/last_transaction_entity.dart';
import 'package:mabank/domain/repositories/last_transaction_repository.dart';

class LastTransactionGetUseCase
    implements UseCase<List<LastTransactionEntity>, NoParam> {
  final LastTransactionRepository _lastTransactionRepository;

  LastTransactionGetUseCase(this._lastTransactionRepository);
  @override
  Future<List<LastTransactionEntity>> call(NoParam param) {
    return _lastTransactionRepository.getLastTRansaction();
  }
}
