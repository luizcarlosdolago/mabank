import 'package:mabank/commons/usecase.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';
import 'package:mabank/domain/repositories/wallet_repository.dart';

class GetWalletUseCase implements UseCase<WalletEntity, NoParam> {
  final WalletRepository _walletRepository;

  GetWalletUseCase(this._walletRepository);
  @override
  Future<WalletEntity> call(NoParam param) {
    return _walletRepository.getWallet();
  }
}
