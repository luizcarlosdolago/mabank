class NotificationEntity {
  final String date;
  final bool wasRead;
  final String type;
  final String icon;
  final String message;
  NotificationEntity(
    this.date,
    this.wasRead,
    this.type,
    this.icon,
    this.message,
  );
}
