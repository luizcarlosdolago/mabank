import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class LastTransactionEntity {
  final String icon;
  final String title;
  final String subtitle;
  final double value;
  final String textValue;
  final Color color;
  final String date;

  LastTransactionEntity(this.icon, this.title, this.subtitle, this.value,
      this.color, this.date, this.textValue);

  DateTime toDate() {
    var newFormat = DateFormat("dd/MM/yyyy");
    return newFormat.parse(date);
  }
}
