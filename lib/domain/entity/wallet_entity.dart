class WalletEntity {
  final String status;
  final String balance;
  final String brand;

  WalletEntity(this.status, this.balance, this.brand);
}
