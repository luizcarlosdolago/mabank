class ProfileEntity {
  final String image;
  final String name;
  final String email;

  ProfileEntity(
    this.image,
    this.name,
    this.email,
  );
}
