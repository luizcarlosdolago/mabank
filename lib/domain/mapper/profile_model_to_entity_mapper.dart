import 'package:mabank/data/model/profile_model.dart';
import 'package:mabank/domain/entity/profile_entity.dart';

class ProfileModelToEntityMapper {
  final ProfileModel _model;

  ProfileModelToEntityMapper(this._model);

  ProfileEntity toEntity() {
    return ProfileEntity(_model.image,_model.email,_model.name);
  }
}
