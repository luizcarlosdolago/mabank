import 'package:mabank/data/model/notification_model.dart';
import 'package:mabank/domain/entity/notification_entity.dart';

class NotificationModelToEntityMapper {
  final NotificationModel _model;

  NotificationModelToEntityMapper(this._model);

  NotificationEntity toEntity() {
    return NotificationEntity(
        _model.date, _model.wasRead, _model.type, _model.icon, _model.message);
  }
}
