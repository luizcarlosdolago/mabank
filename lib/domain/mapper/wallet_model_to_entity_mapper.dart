import 'package:mabank/data/model/wallat_model.dart';
import 'package:mabank/domain/entity/wallet_entity.dart';

class WalletModelToEntityMapper {
  final WalletModel _model;
  WalletModelToEntityMapper(this._model);

  WalletEntity toEntity() {
    return WalletEntity(_model.status, _model.balance, _model.brand);
  }
}
