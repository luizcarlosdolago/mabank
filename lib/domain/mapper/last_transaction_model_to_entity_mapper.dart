import 'package:mabank/data/model/last_transaction_model.dart';
import 'package:mabank/domain/entity/last_transaction_entity.dart';

class LastTransactionModelToEntityMapper {
  final LastTransactionModel _model;

  LastTransactionModelToEntityMapper(this._model);

  LastTransactionEntity toEntity() {
    return LastTransactionEntity(_model.icon, _model.title, _model.subtitle,
        _model.value, _model.color, _model.date, _model.date);
  }
}
