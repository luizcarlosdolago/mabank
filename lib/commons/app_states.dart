abstract class AppState {}

class InitialAppState implements AppState {}

class LoadingAppState implements AppState {}

class ErrorAppState implements AppState {
  final String? message;
  ErrorAppState({this.message});
}

class DataAppState<T> implements AppState {
  final T data;

  DataAppState(this.data);
}
