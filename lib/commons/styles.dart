import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//POPPINS

TextStyle kHeadingLargeFont = GoogleFonts.poppins(
  fontWeight: FontWeight.w600,
  fontSize: 24,
);

TextStyle kTextHeadingMediumFont = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
  fontSize: 18,
);

TextStyle kHeadingMediumFont = GoogleFonts.poppins(
  fontWeight: FontWeight.w500,
  fontSize: 18,
);

//QUICKSAND

TextStyle kTextInfoSection = GoogleFonts.quicksand(
  fontWeight: FontWeight.w700,
  fontSize: 24,
);

TextStyle kTextMediumFont = GoogleFonts.quicksand(
  fontWeight: FontWeight.w600,
  fontSize: 16,
);

TextStyle kTextQuicksandMediumFont = GoogleFonts.quicksand(
  fontWeight: FontWeight.w500,
  fontSize: 14.63,
);

TextStyle kQuicksandFont = GoogleFonts.quicksand(
  fontWeight: FontWeight.w500,
  fontSize: 13.0,
);
