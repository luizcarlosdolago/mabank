import 'package:flutter/material.dart';
import 'package:mabank/commons/colors.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  final ValueChanged<int> ontap;
  final List<BottomNavigationBarItem> items;
  const BottomNavigationBarWidget(
      {Key? key, required this.items, required this.ontap})
      : super(key: key);

  @override
  State<BottomNavigationBarWidget> createState() =>
      _BottomNavigationBarWidgetState();
}

class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
        color: kColorDarkPurple1,
      ),
      width: MediaQuery.of(context).size.width,
      height: 78,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: _creatItems(),
      ),
    );
  }

  List<Widget> _creatItems() {
    List<Widget> bottomBar = [];
    for (var idx = 0; idx < widget.items.length; idx++) {
      BottomNavigationBarItem item = widget.items[idx];
      bottomBar.add(
        IconButton(
          onPressed: () {
            widget.ontap.call(idx);
            selectedIndex = idx;
          },
          icon: _creatIcons(idx, item),
        ),
      );
    }
    return bottomBar;
  }

  Widget _creatIcons(int index, BottomNavigationBarItem item) {
    if (selectedIndex == index) {
      return item.icon;
    }
    return Opacity(
      opacity: 0.30,
      child: item.icon,
    );
  }
}
